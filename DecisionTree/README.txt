Team name
Prime

Members
Jian Chen
Ankur Jain
Dian Wen
Xu Zhao

Project description
This project is an implementation of C4.5.

Usage
java Main train.arff

Reference links / materials
https://en.wikipedia.org/wiki/C4.5_algorithm
http://blog.csdn.net/xuxurui007/article/details/18045943
https://www.cs.umd.edu/sites/default/files/scholarly_papers/fatih-kaya_1.pdf
http://blog.csdn.net/v_july_v/article/details/7577684