/*
 * this class handle important information and behavior of instances in a given dataSet
 */

package c45;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;

public class DataSet {
	// all the attributes of instances
	public ArrayList<Attribute> attributes = new ArrayList<>();
	//all the attributes that waiting for processing of instances
	public ArrayList<Integer> validAttributes = new ArrayList<>();
	// all the instances of a given dataSet
	public ArrayList<Instance> instances = new ArrayList<>();

	/**
	 * contruct a instances object using the instances object of weka to load data
	 * @param instances
	 */
	public DataSet(weka.core.Instances instances) {
		//get attribute
		for (int i = 0; i < instances.numAttributes(); ++i) {
			weka.core.Attribute attributeSrc = instances.attribute(i);
			Attribute attributeDst = new Attribute();
			attributes.add(attributeDst);

			//assign discrete attribute value
			attributeDst.name = attributeSrc.name();
			Enumeration<?> enums = attributeSrc.enumerateValues();
			while (enums != null && enums.hasMoreElements()) {
				attributeDst.addValue(enums.nextElement());
			}

			// exclude label
			if (i != instances.numAttributes() - 1) {
				validAttributes.add(i);
			}
		}
		
		//get instances
		for (int i = 0; i < instances.numInstances(); i++) {
			weka.core.Instance instanceSrc = instances.instance(i);
			c45.Instance instanceDst = new c45.Instance();
			for (int j = 0; j < instances.numAttributes(); j++) {
				if (attributes.get(j).isContinuous()) {
					instanceDst.addValue(instanceSrc.value(j));
				} else {
					instanceDst.addValue(attributes.get(j).toInt(
							instanceSrc.stringValue(j)));
				}
			}
			this.instances.add(instanceDst);
		}
	}

	/**
	 * copy constructor
	 * @param origin
	 */
	public DataSet(DataSet origin) {
		this.attributes = new ArrayList<>(origin.attributes);
		this.validAttributes = new ArrayList<>(origin.validAttributes);
		this.instances = new ArrayList<>(origin.instances);
	}

	/**
	 * get all the instances that its value of a given attribute equals to a given value.
	 * @param attributeIndex
	 * @param value
	 * @return
	 */
	public DataSet equalInstances(int attributeIndex, int value) {
		DataSet instances = new DataSet(this);
		instances.instances.clear();
		for (Instance instance : this.instances) {
			if (instance.values.get(attributeIndex).intValue == value) {
				instances.instances.add(instance);
			}
		}
		return instances;
	}

	/**
	 * get all the instances in a given instances set that its value of a given attribute equals to a given value.
	 * @param instances
	 * @param attributeIndex
	 * @param value
	 * @return
	 */
	public static ArrayList<Instance> equalInstances(List<Instance> instances,
			int attributeIndex, int value) {
		ArrayList<Instance> matchedItems = new ArrayList<>();
		for (Instance instance : instances) {
			if (instance.values.get(attributeIndex).intValue == value) {
				matchedItems.add(instance);
			}
		}
		return matchedItems;
	}

	/**
	 * get all the instances that its value of a given attribute is larger than a given value.
	 * @param attributeIndex
	 * @param value
	 * @return
	 */
	public DataSet greaterInstances(int attributeIndex, double value) {
		DataSet instances = new DataSet(this);
		instances.instances.clear();
		for (Instance instance : this.instances) {
			if (instance.values.get(attributeIndex).doubleValue > value) {
				instances.instances.add(instance);
			}
		}
		return instances;
	}

	/**
	 * get all the instances that its value of a given attribute is less than or equals to a given value.
	 * @param attributeIndex
	 * @param value
	 * @return
	 */	
	public DataSet lowerOrEqualInstances(int attributeIndex, double value) {
		DataSet instances = new DataSet(this);
		instances.instances.clear();
		for (Instance instance : this.instances) {
			if (instance.values.get(attributeIndex).doubleValue <= value) {
				instances.instances.add(instance);
			}
		}
		return instances;
	}

	/**
	 * get the attribute index of label
	 * @return
	 */
	public int getLabelIndex() {
		return attributes.size() - 1;
	}

	/**
	 * Discretize all the continuous attributes that still need to be processed into discrete attributes
	 */
	public void discretize() {
		for (Integer attributeIndex : validAttributes) {
			Attribute attribute = attributes.get(attributeIndex);
			if (attribute.isContinuous()) {
				attribute.threshold = getThreshold(attributeIndex);
				for (Instance instance : instances) {
					Value value = instance.values.get(attributeIndex);
					//if the value is larger than the threshold, set 1 as the int value of the instance, otherwise it's 0.
					if (value.doubleValue > attribute.threshold) {
						value.intValue = 1;
					} else {
						value.intValue = 0;
					}
				}
			}
		}
	}

	/**
	 * Calculate the threshold value of a given attribute of the current instances object
	 */
	private double getThreshold(final int attributeIndex) {
		ArrayList<Instance> instancesCopy = new ArrayList<>(instances);
		//sort all the values of the given attribute of this instances
		Collections.sort(instancesCopy, new Comparator<Instance>() {

			@Override
			public int compare(Instance o1, Instance o2) {
				double o1Value = o1.values.get(attributeIndex).doubleValue;
				double o2Value = o2.values.get(attributeIndex).doubleValue;
				return ((Double) o1Value).compareTo(o2Value);
			}
		});

		double threshold = 0;
		double minEntropy = Double.MAX_VALUE;
		for (int i = 1; i < instancesCopy.size(); ++i) {
			//divide the instances into two parts
			List<Instance> subset1 = instancesCopy.subList(0, i);
			List<Instance> subset2 = instancesCopy.subList(i,
					instancesCopy.size());
			//calculate the entropy of the two subSet
			double entropy = subset1.size()
					* DecisionTree.entropy(subset1, getLabelIndex())
					/ instancesCopy.size() + subset2.size()
					* DecisionTree.entropy(subset2, getLabelIndex())
					/ instancesCopy.size();
			//choose the minimum entropy and use the value of the splitting index as threshold
			if (entropy < minEntropy) {
				minEntropy = entropy;
				threshold = instancesCopy.get(i - 1).get(attributeIndex).doubleValue;
			}
		}
		return threshold;
	}

	/**
	 * Check the label of the instances, if their labels are all the same, return the label. Otherwise return null
	 * @param instances
	 * @return
	 */
	public Integer getOnlyLabel(DataSet instances) {
		Integer label = instances.instances.get(0).getLabel();
		for (Instance instance : instances.instances) {
			if (instance.getLabel() != label) {
				return null;
			}
		}
		return label;
	}

	/**
	 * Check the dominate label of the instances, return the most dominate labels
	 * @param instances
	 * @return
	 */
	public int getDominateLabel(DataSet instances) {
		Attribute label = instances.attributes
				.get(instances.attributes.size() - 1);
		int maxTimes = 0;
		int dominateLabel = 0;
		int[] valueDistribute = new int[label.values.size()];
		for (Instance instance : instances.instances) {
			valueDistribute[instance.getLabel()]++;
			if (valueDistribute[instance.getLabel()] > maxTimes) {
				maxTimes = valueDistribute[instance.getLabel()];
				dominateLabel = instance.getLabel();
			}
		}
		return dominateLabel;
	}
}
