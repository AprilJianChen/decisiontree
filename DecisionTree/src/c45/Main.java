package c45;

import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Main {
	public static boolean DEBUG = false;

	/**
	 * The main method
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) {
		String trainDataFile = null;
		if (args == null || args.length == 0) {
			if (DEBUG) {
				//trainDataFile = "trainProdIntro.binary.rand.arff";
				 trainDataFile = "trainProdSelection.rand.arff";
			} else {
				System.out.println("Usage: java Main INPUT_FILE");
				return;
			}
		} else {
			trainDataFile = args[0];
		}

		DataSource dataSource = null;
		Instances instances = null;
		try {
			dataSource = new DataSource(trainDataFile);
			instances = dataSource.getDataSet();
			DecisionTree dt = new DecisionTree(instances);
			dt.pruneThreshold = 2;
			dt.build();
			dt.print();
			dt.crossValidation();
		} catch (Exception e) {
			System.out.println("Illegal data file");
			return;
		}

	}
}
