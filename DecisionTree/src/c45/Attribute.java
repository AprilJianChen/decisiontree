/**
 * This class recode important information of attributes
 */

package c45;

import java.util.HashMap;
import java.util.Map;

public class Attribute {
	public String name = null;
	public Map<Object, Integer> values = new HashMap<>();
	public Map<Integer, String> nameOfValues = new HashMap<>();
	public double threshold = 0;  //used to discretize this attribute

	/**
	 * check if it's continuous attribute
	 * @return
	 */
	public boolean isContinuous() {
		return values.isEmpty();
	}

	/**
	 * map discrete attribute's actual value to integer, like student-1, professor-2
	 * @param value
	 */
	public void addValue(Object value) {
		int intValue = values.size();
		values.put(value, intValue);
		nameOfValues.put(intValue, value.toString());
	}

	/**
	 * get int value with actual value
	 * @param value
	 * @return
	 */
	public int toInt(Object value) {
		return values.get(value);
	}

	/**
	 * get attribute's name
	 * @param value
	 * @return
	 */
	public String getName(int value) {
		return nameOfValues.get(value);
	}

}
