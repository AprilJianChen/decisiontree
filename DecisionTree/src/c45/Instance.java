// This class handle issues of instance

package c45;

import java.util.ArrayList;

public class Instance {
	public ArrayList<Value> values = new ArrayList<>();

	/**
	 * use attribute index to get the attribute value of the instance
	 * @param attribute
	 * @return
	 */
	public Value get(int attribute) {
		return values.get(attribute);
	}

	/**
	 * add double value of continuous attribute
	 * @param value
	 */
	public void addValue(double value) {
		values.add(new Value(value));
	}

	/** 
	 * add int value of discrete attribute
	 * @param value
	 */
	public void addValue(int value) {
		values.add(new Value(value));
	}

	/**
	 * get the int value of the label of the instance
	 * @return
	 */
	public int getLabel() {
		return values.get(values.size() - 1).intValue;
	}

	/**
	 * Print this object
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (Value value : values) {
			sb.append(value.doubleValue).append("/").append(value.intValue)
					.append(", ");
		}
		sb.append("]");
		return sb.toString();
	}
}
