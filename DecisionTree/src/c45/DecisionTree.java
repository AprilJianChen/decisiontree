/*
 * this class handles the main issues of C4.5 algorithm, like building a tree.
 */

package c45;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DecisionTree {
	TreeNode root; 
	DataSet dataSet;

	/**
	 * construct a new decisionTree object
	 * @param instances
	 */
	public DecisionTree(weka.core.Instances instances) {
		this.dataSet = new DataSet(instances);
	}

	/**
	 * 10 fold cross validation
	 */
	public void crossValidation() {
		// Collections.shuffle(instances.instances);
		DataSet copy = new DataSet(dataSet);

		int pieces = 10;
		int countsPerPiece = dataSet.instances.size() / pieces;
		int failedCount = 0;
		int totalCount = 0;
		for (int i = 0; i <= pieces; ++i) {
			dataSet = new DataSet(copy);
			int endIndex = Math.min((i + 1) * countsPerPiece,
					dataSet.instances.size());
			List<Instance> testCases = new ArrayList<Instance>(
					dataSet.instances.subList(i * countsPerPiece, endIndex));
			dataSet.instances.removeAll(testCases);
			build();

			for (Instance instance : testCases) {
				int label = find(instance);
				if (label != instance.getLabel()) {
					failedCount++;
				}
				totalCount++;
			}
		}

		// output cross validation result
		double incorrectRatio = failedCount * 1.0 / copy.instances.size();
		double correctRatio = 1 - incorrectRatio;
		System.out.println("=== Stratified cross-validation ===");
		System.out.println("=== Summary ===");
		System.out.println("Correctly Classified Instances\t\t"
				+ (totalCount - failedCount) + "\t\t" + correctRatio * 100
				+ " %");
		System.out.println("Incorrectly Classified Instances\t" + failedCount
				+ "\t\t" + incorrectRatio * 100 + " %");
		System.out.println("Total Number of Instances\t\t" + totalCount);
	}

	/**
	 * classify a test instance
	 * @param instance
	 * @return
	 */
	public int find(Instance instance) {
		return findSubTree(root, instance);
	}

	/**
	 * Recursion to classify a test instance
	 * @param node
	 * @param instance
	 * @return
	 */
	private int findSubTree(TreeNode node, Instance instance) {
		if (node == null) {
			return -1;
		}

		//if it's leaf node, return label
		if (node.childNode.size() == 0) {
			return node.label;
		}

		// if the size of condition field of a node is 0, it's a continuous attribute and it should have two childNodes. 
		// Use threshold of the node to determine which subtree to go.
		Value value = instance.get(node.attributeIndex);
		if (node.conditions.size() == 0) {
			if (value.doubleValue <= node.threshold) {
				return findSubTree(node.childNode.get(0), instance);
			} else {
				return findSubTree(node.childNode.get(1), instance);
			}
		}

		//if the size of condition field of a node is not 0, it's a discrete attribute and it should have multiple childNodes.
		// use the int value of the attribute of the instance to match with the conditions of the node, and determine which subtree to go.
		for (int i = 0; i < node.conditions.size(); ++i) {
			if (value.intValue == node.conditions.get(i)) {
				return findSubTree(node.childNode.get(i), instance);
			}
		}
		return -1;
	}

	/**
	 * build a decision tree
	 */
	public void build() {
		root = buildSubTree(dataSet);
	}

    //define prune threshold to do pre-pruning
	public int pruneThreshold = 2;

	/**
	 * recursion to build a decision tree
	 * @param instances
	 * @return
	 */
	private TreeNode buildSubTree(DataSet instances) {
		//if the dataSet is empty
		if (instances.instances.size() == 0) {
			return null;
		}

		//if the instances has same label
		Integer label = instances.getOnlyLabel(instances);
		if (label != null) {
			return TreeNode.newLeaf(label);
		}

		//if there is no attribute waiting to be processed or the size of remaining instances reaches the prune threshold
		if (instances.validAttributes.size() == 0
				|| instances.instances.size() <= pruneThreshold) {
			label = instances.getDominateLabel(instances);
			return TreeNode.newLeaf(label);
		}

		instances = new DataSet(instances);
		instances.discretize(); // discretize all the continuous instances 

		//get the index of attribute that its IGR is largest, and use this attribute to build a new node.
		// remove the index of the attribute from waiting-for-process attribute list
		int bestIndex = getIndexOfMaxIGR(instances);
		instances.validAttributes.remove((Object) bestIndex); 

		Attribute attribute = instances.attributes.get(bestIndex);
		TreeNode treeNode = TreeNode.newBranch(bestIndex);
		/*Set information for new node based on the attribute type. 
		*If it's a continuous attribute, set threshold and divide the instances into two parts to build two subtree
		* Otherwise, add the int value of the discrete attribute into condition list and divide the instances into multiple
		* parts according to different values, then uses these multiple instances set to build subtrees.
		*/
		if (attribute.isContinuous()) {
			treeNode.threshold = attribute.threshold;
			DataSet lowerSet = instances.lowerOrEqualInstances(bestIndex,
					attribute.threshold);
			DataSet greaterSet = instances.greaterInstances(bestIndex,
					attribute.threshold);
			treeNode.childNode.add(buildSubTree(lowerSet));
			treeNode.childNode.add(buildSubTree(greaterSet));
		} else {
			for (Object key : attribute.values.keySet()) {
				Integer value = attribute.values.get(key);
				DataSet subset = instances.equalInstances(bestIndex, value);
				if (subset.instances.isEmpty()) {
					continue;
				}
				treeNode.conditions.add(value);
				treeNode.childNode.add(buildSubTree(subset));
			}
		}
		return treeNode;
	}

	/**
	 * use the formula(IGR = IG/IV) to calculate IGR of all the valid attributes, 
	 * then return the attribute index that its IGR is largest.
	 * @param instances
	 * @return
	 */
	private int getIndexOfMaxIGR(DataSet instances) {
		int bestIndex = instances.validAttributes.get(0);
		double entropy = DecisionTree.entropy(instances.instances,
				instances.getLabelIndex());
		double maxInformationGainRatio = Double.MIN_VALUE;
		for (Integer attributeIndex : instances.validAttributes) {
			double weightedEntropy = entropy(instances.instances,
					instances.getLabelIndex(), attributeIndex);
			//get information gain (gain(Attribute) = info(D) - info(Attribute))
			double gain = entropy - weightedEntropy;
			double splitInformation = entropy(instances.instances,
					attributeIndex);
			double informationGainRatio = gain / splitInformation;
			if (informationGainRatio > maxInformationGainRatio) {
				maxInformationGainRatio = informationGainRatio;
				bestIndex = attributeIndex;
			}
		}
		return bestIndex;
	}

	/**
	 * Print cross validation information
	 */
	public void print() {
		System.out.println("=== Run information ===");
		System.out.println("Instances:\t" + this.dataSet.instances.size());
		System.out.println("Attributes:\t" + this.dataSet.attributes.size());
		for (Attribute attribute : this.dataSet.attributes) {
			System.out.println("\t\t" + attribute.name);
		}
		System.out.println("Test mode:10-fold cross-validation");
		System.out.println("=== Classifier model (full training set) ===");
		long beginTime = System.currentTimeMillis();
		printTree(root, 0);
		long endTime = System.currentTimeMillis();
		System.out.println();
		System.out.println("Time taken to build model: "
				+ (endTime - beginTime) * 1.0 / 1000 + " seconds");
		System.out.println();
	}

	/**
	 * print the subtree of a given node
	 * @param node
	 * @param height
	 */
	private void printTree(TreeNode node, int height) {
		if (node == null) {
			System.out.println();
			return;
		}

		// leaf
		if (node.childNode.size() == 0) {
			Attribute label = dataSet.attributes.get(dataSet
					.getLabelIndex());
			System.out.println(": " + label.getName(node.label));
			return;
		}

		Attribute attribute = dataSet.attributes.get(node.attributeIndex);
		System.out.println();
		// two branches
		if (node.conditions.size() == 0) {
			System.out.print(spaceOfHeight(height).append(attribute.name)
					.append(" <= ").append(node.threshold));
			printTree(node.childNode.get(0), height + 1);
			System.out.print(spaceOfHeight(height).append(attribute.name)
					.append(" > ").append(node.threshold));
			printTree(node.childNode.get(1), height + 1);
			return;
		}
        //multiple branches
		for (int i = 0; i < node.childNode.size(); ++i) {
			System.out.print(spaceOfHeight(height).append(attribute.name)
					.append(" = ")
					.append(attribute.getName(node.conditions.get(i))));
			printTree(node.childNode.get(i), height + 1);
		}
	}

	/**
	 * used to format the output
	 * @param height
	 * @return
	 */
	private StringBuilder spaceOfHeight(int height) {
		StringBuilder sb = new StringBuilder();
		while (height-- > 0) {
			sb.append("|  ");
		}
		return sb;
	}

	/**
	 * calculate the entropy of the attribute (info(Attribute))
	 * @param instances
	 * @param targetAttribute
	 * @param weightAttribute
	 * @return
	 */
	private static double entropy(List<Instance> instances,
			int targetAttribute, int weightAttribute) {
		Map<Integer, Integer> caseCounts = countValues(instances,
				weightAttribute);
		double entropySum = 0;
		for (Integer value : caseCounts.keySet()) {
			Integer count = caseCounts.get(value);
			ArrayList<Instance> matchItems = DataSet.equalInstances(
					instances, weightAttribute, value);
			double p = count * 1.0 / instances.size();
			double entropy = entropy(matchItems, targetAttribute);
			entropySum += p * entropy;
		}
		return entropySum;
	}

	/**
	 * used to calculate the entropy of the dataSet (info(D)) or the split Information (IV)
	 * @param instances
	 * @param targetAttribute
	 * @return
	 */
	public static double entropy(List<Instance> instances, int targetAttribute) {
		Map<Integer, Integer> caseCounts = countValues(instances,
				targetAttribute);
		double entropySum = 0;
		for (Integer value : caseCounts.keySet()) {
			Integer count = caseCounts.get(value);
			double p = count * 1.0 / instances.size();
			double entropy = -p * (Math.log(p) / Math.log(2));
			entropySum += entropy;
		}
		return entropySum;
	}

	/**
	 * calculate the number of different values of a given attribute
	 * @param instances
	 * @param targetAttribute
	 * @return
	 */
	private static Map<Integer, Integer> countValues(List<Instance> instances,
			int targetAttribute) {
		Map<Integer, Integer> caseCounts = new HashMap<>();
		for (Instance instance : instances) {
			int value = instance.get(targetAttribute).intValue;
			Integer count = caseCounts.get(value);
			if (count == null) {
				caseCounts.put(value, 1);
				continue;
			}
			caseCounts.put(value, count + 1);
		}
		return caseCounts;
	}

}
