/*
 * This class has information of tree nodes.
 */

package c45;

import java.util.ArrayList;

public class TreeNode {
	public int label;  //used for leaf node to show final result

	public int attributeIndex;   // index of the attribute of the node
	public double threshold;   // threshold value of the two child nodes of continuous attributes
	public ArrayList<Integer> conditions = new ArrayList<>();   // conditions of branches of discrete attribute
	public ArrayList<TreeNode> childNode = new ArrayList<>(); 

	/**
	 * construct a new leaf node with label
	 * @param label
	 * @return
	 */
	public static TreeNode newLeaf(int label) {
		TreeNode treeNode = new TreeNode();
		treeNode.label = label;
		return treeNode;
	}

	/**
	 *construct a new branch node with attribute index 
	 * @param attributeIndex
	 * @return
	 */
	public static TreeNode newBranch(int attributeIndex) {
		TreeNode treeNode = new TreeNode();
		treeNode.attributeIndex = attributeIndex;
		return treeNode;
	}

	private TreeNode() {
	}

}
