/*this class is used to handle the two likely kind value of attributes. 
 * For example, if it's a continuous attribute, it has double value.
 * If it's a discrete attribute, it would have an integer value map to its actual value.
 */

package c45;

public class Value {
	public double doubleValue;
	public int intValue;

	/**
	 * construct with double value
	 * @param value
	 */
	public Value(double value) {
		this.doubleValue = value;
	}

	/**
	 * construct with int value
	 * @param value
	 */
	public Value(int value) {
		this.intValue = value;
	}
}
